/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


'use strict';
var app = angular.module('myApp');
app.controller('PartnersFromBeaconCtrl', ['$scope', '$modal', 'beaconId', 'cb', '$route',
    'PartnerService', 'BeaconService', 'ErrorCMDService', 'SessionService',
    function ($scope, $modal, beaconId, cb, $route,
	    PartnerService, BeaconService, ErrorCMDService, SessionService) {

	$scope.getAdmin = function ()
	{
	    return $cookieStore.get('user').isAdmin;
	};

	//$scope.isAdmin = $scope.getAdmin();
        $scope.isAdmin = true;


	$scope.addPartner = function () {
            
	    $modal.open({
		templateUrl: 'views/addpartnertobeacon2.html',
		controller: 'PartnerListCtrl2',
		resolve: {
		    beaconId: function () {
			return beaconId;
		    },
		    cb: function () {
			return $scope.updateDisplay;
		    }
		}
	    });
	};
	$scope.viewPartnerDetail = function (partnerId)
	{
	    var resp = PartnerService.getPartner({
		partnerId: partnerId
	    }, function ()
	    {
		if (resp.error === 0)
		{
		    $modal.open({
			templateUrl: 'views/partnerdetail.html',
			controller: 'PartnerDetailCtrl',
			resolve: {
			    partner: function () {
				var partner = resp.data;
				partner.partnerId = partnerId;			
				return partner;
			    },
			    cb: function () {
				return $scope.updateDisplay;
			    }
			}
		    });
		}
		else
		{
		    ErrorCMDService.displayError(resp.message);
		}

	    });
	};
	$scope.removePartnerFromBeacon = function (partnerId)
	{

	    var cb = function () {
		var resp = BeaconService.removePartner({
                    beaconId: beaconId,
		    partnerId: partnerId

		}, function () {

		    if (resp.error === 0)
		    {
			$scope.updateDisplay();
		    }
		    else
		    {
			ErrorCMDService.displayError(resp.message);
		    }
		});
	    };
	    $modal.open({
		templateUrl: 'views/submitmodal.html',
		controller: 'SubmitModalCtrl',
		size: 'sm',
		resolve: {
		    cb: function () {
			return cb;
		    }
		}
	    });
	};
	$scope.pageChanged = function ()
	{
	    var start = ($scope.currentPage - 1) * $scope.itemPerPage;
	    var apiResp = BeaconService.listPartner({
                beaconId: beaconId,
		start: start,
		length: $scope.itemPerPage

	    }, function () {

		if (apiResp.error === 0) {
		    var partners = apiResp.data.partners;
//		    for (var i = 0; i < beacons.length; ++i) {
//			var d = new Date(beacons[i].createTime * 1000);
//			beacons[i].dateCreated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
//			d = new Date(beacons[i].lastUpdate * 1000);
//			beacons[i].dateUpdated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
//		    }
		    $scope.partnerList = partners;
		    $scope.partners = [].concat($scope.partnerList);
		    $scope.totalItems = apiResp.data.total;
                    //$scope.totalItems = beacons.length;
		    $scope.totalPages = $scope.totalItems / $scope.itemPerPage + 1;
		}
		else
		{
		    ErrorCMDService.displayError(apiResp.message);
		}
	    });
	};
	$scope.updateDisplay = function () {
	    $scope.pageChanged();
	};
	$scope.setPage = function (pageNo) {
	    $scope.currentPage = pageNo;
	};
	$scope.init = function () {

	    $scope.$route = $route;
	    //Pagination
	    $scope.totalItems = 0;
	    $scope.itemPerPage = 10;
	    $scope.currentPage = 1;
	    var sessionId = SessionService.getSessionId();
	    if (sessionId)
	    {
		$scope.updateDisplay();
	    } else {
                $scope.updateDisplay();
            }
	};
	$scope.init();
    }
]);