/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


'use strict';
var app = angular.module('myApp');
app.controller('AddPartnerToBeaconCtrl', ['$scope', '$modalInstance', 'beaconId', 'cb',
    'PartnerService', 'BeaconService', 'ErrorCMDService',
    function ($scope, $modalInstance, beaconId, cb,
	    PartnerService, BeaconService, ErrorCMDService) {

	$scope.newPartnerId = -1;
	$scope.init = function ()
	{

	};
	$scope.init();


	$scope.ok = function () {
	    $modalInstance.close($scope.localPartner);
	};
	$scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	};

	$scope.msgList = [];
	$scope.clearMsg = function () {
	    $scope.msgList = [];
	};
	$scope.showMsg = function (msg) {
	    $scope.msgList = [{type: 'danger', content: msg}];
	};


	$scope.confirm = function () {

	    if ($scope.newPartnerId > 0)
	    {
		var resp = BeaconService.addPartner({
                    partnerId: $scope.newPartnerId,
		    beaconId: beaconId
		}, function () {
		    if (resp.error === 0)
		    {
			$scope.ok();
			cb();
		    }
		    else
		    {
			//ErrorCMDService.displayError(resp.error);
                        $scope.showMsg('error: ' + resp.error + ' ' + resp.message);
		    }

		});
	    }
	    else
	    {
		$scope.showMsg('Input partnerId please!');
	    }
	}
	;
    }
]);