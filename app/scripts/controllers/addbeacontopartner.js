/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


'use strict';
var app = angular.module('myApp');
app.controller('AddBeaconToPartnerCtrl', ['$scope', '$modalInstance', 'partnerId', 'cb',
    'PartnerService', 'BeaconService', 'ErrorCMDService',
    function ($scope, $modalInstance, partnerId, cb,
            PartnerService, BeaconService, ErrorCMDService) {

        $scope.newBeaconId = -1;
        


        $scope.ok = function () {
            $modalInstance.close($scope.localPartner);
        };
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.msgList = [];
        $scope.clearMsg = function () {
            $scope.msgList = [];
        };
        $scope.showMsg = function (msg) {
            $scope.msgList = [{type: 'danger', content: msg}];
        };


        $scope.confirm = function () {

            if ($scope.newBeaconId > 0)
            {
                var resp = BeaconService.addPartner({
                    partnerId: partnerId,
                    beaconId: $scope.newBeaconId
                }, function () {
                    if (resp.error === 0)
                    {
                        $scope.ok();
                        cb();
                    }
                    else
                    {
                        //ErrorCMDService.displayError(resp.error);
                        $scope.showMsg('error: ' + resp.error + ' ' + resp.message);
                    }

                });
            }
            else
            {
                $scope.showMsg('Input beaconId please!');
            }
        };
        
        $scope.updateDisplay = function () {
            $scope.pageChanged();
        };
        
        $scope.pageChanged = function ()
        {
            var start = ($scope.currentPage - 1) * $scope.itemPerPage;
            var apiResp = BeaconService.listBeacon({
                start: start,
                length: $scope.itemPerPage

            }, function () {

                if (apiResp.error === 0) {
                    var beacons = apiResp.data.beacons;
//		    for (var i = 0; i < beacons.length; ++i) {
//			var d = new Date(beacons[i].createTime * 1000);
//			beacons[i].dateCreated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
//			d = new Date(beacons[i].lastUpdate * 1000);
//			beacons[i].dateUpdated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
//		    }
                    $scope.beaconList = beacons;
                    $scope.beacons = [].concat($scope.beaconList);
                    $scope.totalItems = apiResp.data.total;
                    //$scope.totalItems = beacons.length;
                    $scope.totalPages = $scope.totalItems / $scope.itemPerPage + 1;
                }
                else
                {
                    ErrorCMDService.displayError(apiResp.message);
                }
            });
        };
        
        $scope.init = function () {

            //Pagination
            $scope.totalItems = 0;
            $scope.itemPerPage = 10;
            $scope.currentPage = 1;
            $scope.updateDisplay();
        };
        $scope.init();
    }
]);