/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


'use strict';
var app = angular.module('myApp');
app.controller('PartnerListCtrl2', ['$scope', '$modalInstance', '$cookieStore', '$route',
    'PartnerService', 'ErrorCMDService', 'SessionService', 'beaconId', 'cb', 'BeaconService',
    function ($scope, $modalInstance, $cookieStore, $route,
            PartnerService, ErrorCMDService, SessionService, beaconId, cb, BeaconService) {

        $scope.getAdmin = function ()
        {
            return $cookieStore.get('user').isAdmin;
        };

        //$scope.isAdmin = $scope.getAdmin();
        $scope.isAdmin = true;

        $scope.addPartnerToList = function (partner) {
            var index = $scope.partners.indexOf(partner);
            //console.log("index: " + index);
            if (index >= 0) {
                //console.log("remove nhe, truoc khi remove size: " + $scope.partners.length);
                $scope.partners.splice(index, 1);
//                console.log("sau khi remove size: " + $scope.partners.length);
            }
            $scope.addedPartners[$scope.addedPartners.length] = partner;
        };

        $scope.removePartnerToList = function (partner) {
            var index = $scope.addedPartners.indexOf(partner);
            //console.log("index: " + index);
            if (index >= 0) {
//                console.log("remove nhe, truoc khi remove size: " + $scope.addedPartners.length);
                $scope.addedPartners.splice(index, 1);
//                console.log("sau khi remove size: " + $scope.addedPartners.length);
            }
            $scope.partners[$scope.partners.length] = partner;
        };

        $scope.addPartners2Beacon = function () {
            if ($scope.addedPartners.length === 0) {
                //ErrorCMDService.displayError("Ban chua chon partner");
                $scope.showMsg("Ban chua chon partner");
                return;
            }
            $scope.clearMsg();
            var partnerIds = "";
            for (var i = 0; i < $scope.addedPartners.length; i++) {
                var partner = $scope.addedPartners[i];
                if (partnerIds.length === 0)
                    partnerIds = "" + partner.partnerId;
                else
                    partnerIds = partnerIds + "," + partner.partnerId;
            }
//            console.log("partnerIds: " + partnerIds);
            var resp = BeaconService.addPartners({
                    partnerIds: partnerIds,
                    beaconId: beaconId
                }, function () {
                    if (resp.error === 0)
                    {
//                        console.log("ok roi nhe " + partnerIds + beaconId);
                        alert("Thêm thành công");
                        cb();
                        $scope.ok();
                    }
                    else
                    {
                        //ErrorCMDService.displayError(resp.error);
                        alert("Thêm thất bại\n\r message: " + resp.message + " error: " + resp.error);
                        $scope.showMsg('error: ' + resp.error + ' ' + resp.message);
                    }

                });
        };

        $scope.pageChanged = function ()
        {
            var start = ($scope.currentPage - 1) * $scope.itemPerPage;
            var apiResp = PartnerService.listPartner({
                start: start,
                length: $scope.itemPerPage

            }, function () {

                if (apiResp.error === 0) {
                    var partners = apiResp.data.partners;
                    $scope.partnerList = partners;
                    $scope.partners = [].concat($scope.partnerList);
                    $scope.totalItems = apiResp.data.total;
                    //$scope.totalItems = beacons.length;
                    $scope.totalPages = $scope.totalItems / $scope.itemPerPage + 1;
                }
                else
                {
                    ErrorCMDService.displayError(apiResp.message);
                }
            });
        };
        $scope.updateDisplay = function () {
            $scope.pageChanged();
        };
        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.checkboxModel = function (values, id) {

        };
        $scope.msgList = [];
        $scope.clearMsg = function () {
            $scope.msgList = [];
        };
        $scope.showMsg = function (msg) {
            $scope.msgList = [{type: 'danger', content: msg}];
        };

        $scope.addPartner2Beacon = function (partnerId) {
            console.log("ok roi nhe " + partnerId);
            if (partnerId > 0)
            {
                var resp = BeaconService.addPartner({
                    partnerId: partnerId,
                    beaconId: beaconId
                }, function () {
                    if (resp.error === 0)
                    {
                        console.log("ok roi nhe " + partnerId + beaconId);
                        cb();
                    }
                    else
                    {
                        //ErrorCMDService.displayError(resp.error);
                        $scope.showMsg('error: ' + resp.error + ' ' + resp.message);
                    }

                });
            }
            else
            {
                $scope.showMsg('Input partnerId please!');
            }
        };
        
        $scope.ok = function () {
            console.log("modalInstance: " + $modalInstance);
	    $modalInstance.dismiss('cancel');
	};

        $scope.init = function () {

            $scope.$route = $route;
            //Pagination
            $scope.totalItems = 0;
            $scope.itemPerPage = 10;
            $scope.currentPage = 1;
            $scope.addedPartnerList = [];
            $scope.addedPartners = [].concat($scope.addedPartnerList);
            var sessionId = SessionService.getSessionId();
            if (sessionId)
            {
                $scope.updateDisplay();
            } else {
                $scope.updateDisplay();
            }
        };



        $scope.init();
    }
]);