/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


'use strict';
var app = angular.module('myApp');
app.controller('AppListCtrl', ['$cookieStore', '$scope', '$modal', '$route',
    'AppService', 'ErrorCMDService', 'SessionService',
    function ($cookieStore, $scope, $modal, $route,
	    AppService, ErrorCMDService, SessionService) {

	$scope.getAdmin = function ()
	{
	    return $cookieStore.get('user').isAdmin;
	};

	//$scope.isAdmin = $scope.getAdmin();
        $scope.isAdmin = true;


	$scope.createApp = function () {
            
	    $modal.open({
		templateUrl: 'views/appdetail.html',
		controller: 'AppDetailCtrl',
		resolve: {
		    app: function () {
			return undefined;
		    },
		    cb: function () {
			return $scope.updateDisplay;
		    }
		}
	    });
	};
	$scope.viewAppDetail = function (appId)
	{
	    var resp = AppService.getApp({
		appId: appId
	    }, function ()
	    {
		if (resp.error === 0)
		{
		    $modal.open({
			templateUrl: 'views/appdetail.html',
			controller: 'AppDetailCtrl',
			resolve: {
			    app: function () {
				var app = resp.data;
				app.appId = appId;			
				return app;
			    },
			    cb: function () {
				return $scope.updateDisplay;
			    }
			}
		    });
		}
		else
		{
		    ErrorCMDService.displayError(resp.message);
		}

	    });
	};
	$scope.removeBeacon = function (appId)
	{

	    var cb = function () {
		var resp = AppService.removeApp({
		    appId: appId

		}, function () {

		    if (resp.error === 0)
		    {
			$scope.updateDisplay();
		    }
		    else
		    {
			ErrorCMDService.displayError(resp.message);
		    }
		});
	    };
	    $modal.open({
		templateUrl: 'views/submitmodal.html',
		controller: 'SubmitModalCtrl',
		size: 'sm',
		resolve: {
		    cb: function () {
			return cb;
		    }
		}
	    });
	};
	$scope.pageChanged = function ()
	{
	    var start = ($scope.currentPage - 1) * $scope.itemPerPage;
	    var apiResp = AppService.listApp({
		start: start,
		length: $scope.itemPerPage

	    }, function () {

		if (apiResp.error === 0) {
		    var apps = apiResp.data.apps;
//		    for (var i = 0; i < beacons.length; ++i) {
//			var d = new Date(beacons[i].createTime * 1000);
//			beacons[i].dateCreated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
//			d = new Date(beacons[i].lastUpdate * 1000);
//			beacons[i].dateUpdated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
//		    }
		    $scope.appList = apps;
		    $scope.apps = [].concat($scope.appList);
		    $scope.totalItems = apiResp.data.total;
                    //$scope.totalItems = beacons.length;
		    $scope.totalPages = $scope.totalItems / $scope.itemPerPage + 1;
		}
		else
		{
		    ErrorCMDService.displayError(apiResp.message);
		}
	    });
	};
	$scope.updateDisplay = function () {
	    $scope.pageChanged();
	};
	$scope.setPage = function (pageNo) {
	    $scope.currentPage = pageNo;
	};
	$scope.init = function () {

	    $scope.$route = $route;
	    //Pagination
	    $scope.totalItems = 0;
	    $scope.itemPerPage = 10;
	    $scope.currentPage = 1;
	    var sessionId = SessionService.getSessionId();
	    if (sessionId)
	    {
		$scope.updateDisplay();
	    } else {
                $scope.updateDisplay();
            }
	};
	$scope.init();
    }
]);