/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


'use strict';
var app = angular.module('myApp');
app.controller('AddAdvertisementToBeaconCtrl', ['$cookieStore', '$scope', '$modalInstance', '$route',
    'BeaconService', 'AdvertisementService', 'ErrorCMDService', 'SessionService', 'beaconId', 'cb',
    function ($cookieStore, $scope, $modalInstance, $route,
            BeaconService, AdvertisementService, ErrorCMDService, SessionService, beaconId, cb) {

        $scope.getAdmin = function ()
        {
            return $cookieStore.get('user').isAdmin;
        };

        //$scope.isAdmin = $scope.getAdmin();
        $scope.isAdmin = true;
        $scope.showPartnerList = false;
        $scope.beaconId = beaconId;

        $scope.addAdvertisementToList = function (advertisement) {
            var index = $scope.advertisements.indexOf(advertisement);
            //console.log("index: " + index);
            if (index >= 0) {
                //console.log("remove nhe, truoc khi remove size: " + $scope.partners.length);
                $scope.advertisements.splice(index, 1);
//                console.log("sau khi remove size: " + $scope.partners.length);
            }
            index = $scope.addedAdvertisements.indexOf(advertisement);
            if (index < 0)
                $scope.addedAdvertisements[$scope.addedAdvertisements.length] = advertisement;
        };

        $scope.removeAdvertisementToList = function (advertisement) {
            var index = $scope.addedAdvertisements.indexOf(advertisement);
            //console.log("index: " + index);
            if (index >= 0) {
//                console.log("remove nhe, truoc khi remove size: " + $scope.addedPartners.length);
                $scope.addedAdvertisements.splice(index, 1);
//                console.log("sau khi remove size: " + $scope.addedPartners.length);
            }
            index = $scope.advertisements.indexOf(advertisement);
            if (index < 0)
                $scope.advertisements[$scope.advertisements.length] = advertisement;
        };

        $scope.addAdvertisements2Beacon = function () {
            if ($scope.addedAdvertisements.length === 0) {
                //ErrorCMDService.displayError("Ban chua chon partner");
                $scope.showMsg("Bạn chưa chọn mục quảng cáo nào");
                return;
            }
            $scope.clearMsg();
            var adIds = "";
            for (var i = 0; i < $scope.addedAdvertisements.length; i++) {
                var advertisement = $scope.addedAdvertisements[i];
                if (adIds.length === 0)
                    adIds = "" + advertisement.advertisementId;
                else
                    adIds = adIds + "," + advertisement.advertisementId;
            }
//            console.log("partnerIds: " + partnerIds);
            var resp = AdvertisementService.addAdvertisementsToBeacon({
                adIds: adIds,
                beaconId: $scope.beaconId
            }, function () {
                if (resp.error === 0)
                {
//                        console.log("ok roi nhe " + partnerIds + beaconId);
                    alert("Thêm thành công");
                    cb();
                    $scope.ok();
                }
                else
                {
                    //ErrorCMDService.displayError(resp.error);
                    alert("Thêm thất bại\n\r message: " + resp.message + " error: " + resp.error);
                    $scope.showMsg('error: ' + resp.error + ' ' + resp.message);
                }

            });
        };


        $scope.pageChanged = function ()
        {
            var start = ($scope.currentPage - 1) * $scope.itemPerPage;
            var apiResp = AdvertisementService.listAdvertisement({
                start: start,
                length: $scope.itemPerPage

            }, function () {

                if (apiResp.error === 0) {
                    var advertisements = apiResp.data.advertisements;
//		    for (var i = 0; i < beacons.length; ++i) {
//			var d = new Date(beacons[i].createTime * 1000);
//			beacons[i].dateCreated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
//			d = new Date(beacons[i].lastUpdate * 1000);
//			beacons[i].dateUpdated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
//		    }
                    $scope.advertisementList = advertisements;
                    $scope.advertisements = [].concat($scope.advertisementList);
                    $scope.totalItems = apiResp.data.total;
                    //$scope.totalItems = beacons.length;
                    $scope.totalPages = $scope.totalItems / $scope.itemPerPage + 1;
                }
                else
                {
                    //ErrorCMDService.displayError(apiResp.message);
                }
            });
        };

        $scope.msgList = [];
        $scope.clearMsg = function () {
            $scope.msgList = [];
        };
        $scope.showMsg = function (msg) {
            $scope.msgList = [{type: 'danger', content: msg}];
        };

        $scope.ok = function () {
            //console.log("modalInstance: " + $modalInstance);
	    $modalInstance.dismiss('cancel');
	};
        
        $scope.updateDisplay = function () {
            $scope.pageChanged();
        };
//	$scope.setPage = function (pageNo) {
//	    $scope.currentPage = pageNo;
//	};
        $scope.init = function () {

            $scope.$route = $route;
            //Pagination
            $scope.totalItems = 0;
            $scope.itemPerPage = 10;
            $scope.currentPage = 1;
            $scope.addedAdvertisements = [];
            var sessionId = SessionService.getSessionId();
            if (sessionId)
            {
                $scope.updateDisplay();
            } else {
                $scope.updateDisplay();
            }
        };
        $scope.init();
    }
]);